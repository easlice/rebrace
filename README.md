# README #

### What is this? ###

This is a simple perl script to change the bracing style for a given code file.

### What style does it output? ###

It would be better for you to see it for yourself: [Here is the inspiration.](https://twitter.com/UdellGames/status/788690145822306304)