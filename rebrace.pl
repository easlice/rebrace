#!/usr/bin/perl
use strict;
use warnings;

no warnings; # Regex's matching comments may throw warnings
use constant {
	LINE_PADDING => 4,
	TAB_TO_SPACE_COUNT => 4,
	TMP_EXTENSION => '.tmp',
	QUOTED_STRINGS => [qw/ { } ; /],
	#UNQUOTED_STRINGS => [qw/ [^\\\\]\/\/.* /], # C/Java style comments
	UNQUOTED_STRINGS => [qw/ [^\\\\]\#.* /], # Perl style comments
	JOIN_PATTERN => "",
};
use warnings; # be sure to re-enable warnings

my $filepath = shift;
my $line_length = 0;
my $match_regex = "(" . join("|", @{+UNQUOTED_STRINGS}, map {quotemeta} @{+QUOTED_STRINGS}) . ")"; # QUOTED_STRINGS needs to be last

open (my $orig_fh, "<", $filepath) or die "Could not open the file at path '$filepath'";
while (my $line = <$orig_fh>) {
	while ($line =~ s/${match_regex}\s*$//) {}
	my $len = length $line;
	$line_length = $len unless $len <= $line_length;
}
close $orig_fh;

$line_length += LINE_PADDING;

open ($orig_fh, "<", $filepath) or die "Could not open the file at path '$filepath'";
open (my $new_fh, ">", $filepath . TMP_EXTENSION) or die "Could not open tmp file at path '${$filepath . TMP_EXTENSION}'";
while (my $line = <$orig_fh>) {
	chomp $line;
	my @symbols;

	while ($line =~ s/${match_regex}\s*$//) {
		unshift (@symbols, $1);
	}

	my $new_line = $line;
	if (@symbols > 0) {
		# Check for tabs, count as if they were spaces
		my $tab_count = () = $line =~ /\t/g;
		my $this_line_length = $line_length - (TAB_TO_SPACE_COUNT - 1) * $tab_count;
		$new_line = sprintf("%-${this_line_length}s%s", $line, join (JOIN_PATTERN, @symbols));
	}
	print $new_fh $new_line . "\n";
}

close $orig_fh;
close $new_fh;

print "Finished file '$filepath'...\n";
